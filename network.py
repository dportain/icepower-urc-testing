import re
import time
import json
import deepdiff

class Status:
	def __init__(self):
		self.errorText = None
		self.warningText = None
		self.statusText = None
		self.ok = False
	def __repr__(self):
		if self.errorText is not None:
			return '❌ ' + self.errorText
		else:
			if self.warningText is not None:
				return '⚠️  ' + self.warningText
			elif self.ok:
				return '✅ '
			else:
				return ''

def detectCategory(command):
	if command.startswith(r'{"type":"set"'):
		return 'set'
	elif command.startswith(r'{"type":"get"'):
		return 'get'

def stripReplies(response):
	# strip away all the "reply" messages
	newResponse = ''
	messageStart = r'{"type":'
	if type(response) is str:
		if len(response) > 0:
			if 'reply' in response:
				responseGroups = response.split(messageStart)
				while '' in responseGroups:
					responseGroups.remove('')
				responseGroups = [messageStart + r for r in responseGroups] # add back the string fragment that split() removes
				for responseGroup in responseGroups:
					if not r'"type":"reply"' in responseGroup:
						newResponse += responseGroup
			else:
				newResponse = response
	return newResponse

def avertError(response, expectedResponse, status):
	try:
		expectedJSON = json.loads(expectedResponse)
	except ValueError:
		status.errorText = 'Failed to parse JSON: {}\n'.format(response)
		return status
	if len(response) > 0:
		# convert strings to JSON and extract the differences
		if type(response) is str:
			try:
				tree = json.loads(response)
			except ValueError:
				status.errorText = 'Failed to parse JSON: {}\n'.format(response)
				return status
			difference = deepdiff.DeepDiff(tree, expectedJSON)
			if 'values_changed' in difference:
				diffDict = difference['values_changed']
				subKey = list(diffDict)[0]
				diffSubDict = diffDict[subKey]
				expectedValue = diffSubDict['new_value']
				value = diffSubDict['old_value']
				# maybe the two strings are identical numbers with different decimal formats?
				try:
					expectedNumber = float(expectedValue)
					number = float(value)
					if number == expectedNumber:
						status.errorText = None
						status.warningText = 'The value in field {} has a non-spec decimal point format!'.format(subKey)
				# no? oh well
				except:
					return status
	else:
		status.errorText = 'The response came back empty.'
	return status

def verifyGetResponse(response, expectedResponse):

	# try comparing the JSON strings directly
	functionallyIdentical = response == expectedResponse

	# if this doesn't produce a match, dissect the JSON message into its components
	if not functionallyIdentical:
		tagPattern = r'(<[^"<>]+>)' # finds strings that start with <, end with >, and contain at least one letter. The letters ", <, and > are not allowed.

		try:
			expectedJSON = json.loads(expectedResponse)
			referenceJSON = json.loads(response)
		except ValueError:
			return False

		# find the differences between the two dicts
		difference = deepdiff.DeepDiff(expectedJSON, referenceJSON)
		isPlaceholder = []
		if 'dictionary_item_added' in difference or 'dictionary_item_removed' in difference:
			return False
		if 'values_changed' in difference:
			diffDict = difference['values_changed']
			for subKey in list(diffDict):
				diffSubDict = diffDict[subKey]
				expectedValue = diffSubDict['new_value']
				referenceValue = diffSubDict['old_value']

				# check if all different fields are placeholders
				referencePlaceholder = re.search(tagPattern, referenceValue)
				expectedPlaceholder = re.search(tagPattern, expectedValue)
				if expectedPlaceholder is None and referencePlaceholder is None:
					isPlaceholder.append(False)
				else:
					isPlaceholder.append(True)
			if all(isPlaceholder):
				functionallyIdentical = True
		else:
			functionallyIdentical = True

	return functionallyIdentical


def sendCommand(connection, commandData, debugMessages = False):

	delay = 0.2

	command = commandData['command']
	if detectCategory(command) == 'set':
		status = sendSetCommand(connection, commandData, delay, debugMessages)
	elif detectCategory(command) == 'get':
		status = sendGetCommand(connection, commandData, delay, debugMessages)
	return status


def sendGetCommand(connection, commandData, delay, debugMessages):

	status = Status()
	command = commandData['command']
	expectedResponse = commandData['response']
	print('\n' + commandData['name'])
	connection.send(command.encode())
	response = stripReplies(connection.recv().decode())
	logLine = '\nSent GET command:            {}\nExpected status response: {}\nReceived status response: {}'.format(command, expectedResponse, response)
	if verifyGetResponse(response, expectedResponse):
		status.ok = True
		status.statusText = logLine
	else:
		status.errorText = logLine
	if debugMessages:
		print(status.statusText)
	print('get:', status)
	time.sleep(delay)

	return {'get':status}


def sendSetCommand(connection, commandData, delay, debugMessages):

	status = {}
	response = {}
	command = {}

	command['set'] = commandData['command']
	specificationResponse = commandData['response']

	command['get'] = commandData['command'].replace(r'"type":"set"', r'"type":"get"')
	command['confirm'] = command['get']
	errorTemplate = '\nError:\nSent command:         {}\nExpected response: {}\nActual response:   {}'

	status['get'] = Status()
	status['set'] = Status()
	status['confirm'] = Status()
	status['restore'] = Status()

	print('\n' + commandData['name'])

	# typical output:
	# 
	# SET_VOICE_COMP_EQ_LIMITER_RELEASE_TIME
	# get ✅
	# set ✅
	# confirm ⚠️ - The value in field root['input']['audio.sense']['release'] has a non-spec decimal point format!
	# restore ❌ - Restored values were not confirmed correctly!


	# GET command
	
	connection.send(command['get'].encode())
	response['get'] = stripReplies(connection.recv().decode())

	if r'"type":"status"' in response['get']:
		status['get'].ok = True
	else:
		status['get'].errorText = 'Unknown status response to GET command'
	status['get'].statusText = 'Sent GET command:            {}\nReceived correct status response: {}'.format(command['get'], response['get'])
	if debugMessages:
		print(status['get'].statusText)
	print('get:', status['get'])
	time.sleep(delay)


	# SET command
	
	expectedResponse = specificationResponse
	connection.send(command['set'].encode())
	response['set'] = stripReplies(connection.recv().decode())

	if response['set'] == expectedResponse:
		status['set'].ok = True
	else:
		status['set'].errorText = errorTemplate.format(command['set'], expectedResponse, response['set'])
	status['set'].statusText = 'Sent SET command:            {}\nReceived correct status response: {}'.format(command['set'], response['set'])
	if debugMessages:
		print(status['set'].statusText)
	print('set:', status['set'])
	time.sleep(delay * 3)


	# confirm that the last SET command had its intended effect
	
	connection.send(command['confirm'].encode())
	response['confirm'] = stripReplies(connection.recv().decode())

	if response['confirm'] == expectedResponse:
		status['confirm'].ok = True
	else:
		status['confirm'].errorText = errorTemplate.format(command['confirm'], expectedResponse, response['confirm'])
	# try to avert errors by looking at the response in detail
	status['confirm'] = avertError(response['confirm'], expectedResponse, status['confirm'])
	status['confirm'].statusText = 'Sent confirmation query:     {}\nReceived status response: {}'.format(command['confirm'], response['confirm'])
	if debugMessages:
		print(status['confirm'].statusText)
	print('confirm:', status['confirm'])
	time.sleep(delay)


	# restore the original configuration of this command
	if status['get'].ok:
		expectedResponse = response['get']
		command['restore'] = response['get'].replace(r'"type":"status"', r'"type":"set"')
		
		connection.send(command['restore'].encode())
		response['restore'] = stripReplies(connection.recv().decode())

		if response['restore'] != response['get']:
			status['restore'].errorText = errorTemplate.format(command['restore'], expectedResponse, response['restore'])
		else:
			status['restore'].ok = True
		status['restore'] = avertError(response['restore'], expectedResponse, status['restore'])
		status['restore'].statusText = 'Sent restoration command:    {}\nReceived status response: {}'.format(command['restore'], response['restore'])
		if debugMessages:
			print(status['restore'].statusText)
		print('restore:', status['restore'])
		time.sleep(delay)

	return status