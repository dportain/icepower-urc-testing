import socket
import random
import ifaddr

def createPackage(ownIP):

	def convertToUshort(number):
		if number > 65536:
			return None
		elif number < 0:
			return None
		else:
			smallPart = number % 256
			bigPart = (number - smallPart) >> 8
			ushort = bytes([smallPart, bigPart])
			return ushort

	sequence = random.randint(0,200)
	rawSequence = convertToUshort(sequence)
	rawCmdID = b'\x50\xc3'
	rawModelID = b'\x00\x00'
	rawIP = socket.inet_aton(ownIP)
	package = rawSequence + rawCmdID + rawModelID + rawIP

	return package

def interpretResponse(response):

	if b'@' in response:
		responseParts = response.split(b'@')
		if len(responseParts) == 2:
			package, info = responseParts
			# typical content:
			# package: b'\x02\x00S\xc3\x06\xb0\x00\x1f\xb8E\x00\x1e\xc0\xa8\x01\xc5'
			# info: b'avb_souces: 2, avb_sinks: 1\x00\x00HDA_IO\x00\x00\x00\x00'

			if b'\x00\x00' in info:
				infoParts = info.split(b'\x00\x00')
				if len(infoParts) >= 2:
					deviceInfo, deviceName = infoParts[0:2]
					return (deviceName.decode(), deviceInfo.decode())
	return (None, None)

# Network settings
timeout = 0.5
bufferSize = 1024
targetPort = 61510
sourcePort = targetPort
targetIP = '255.255.255.255'

# Generate the magic package
print('\nQuerying devices...')
ownHostname = socket.gethostname()
mainIP = socket.gethostbyname(ownHostname)
package = createPackage(mainIP)

# Finding out our own IPs
ownIPs = set()
adapters = ifaddr.get_adapters()
for adapter in adapters:
	for IP in adapter.ips:
		if type(IP.ip) is str: # IPv4 addresses
			ownIPs.add(IP.ip)
		elif type(IP.ip) is tuple: # IPv6 addresses
			if len(IP.ip) == 3:
				ownIPs.add(IP.ip[0])

# Send out the package
discoverySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
discoverySocket.settimeout(timeout) # wait for n seconds of possible responses
discoverySocket.bind(('', sourcePort))
# discoverySocket.setsockopt(socket.SOL_SOCKET, 25, 'eth0') # we can't specify the network interface without admin rights
discoverySocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
discoverySocket.sendto(package, (targetIP, targetPort))

# Listen for responses
sequence = package[0:2]
discoveredDevices = []
while True:
	response = None
	address = None
	try:
		response, address = discoverySocket.recvfrom(1024)
	except socket.timeout:
		break
	if not len(response):
		break
	else:
		if address is not None and response is not None:
			if len(address) == 2:
				IP, port = address
			if response.startswith(sequence):
				deviceName, deviceInfo = interpretResponse(response)
				discoveredDevice = None
				if deviceName is not None and deviceInfo is not None:
					discoveredDevice = '{}: {} ({})'.format(IP, deviceName, deviceInfo)
				else:
					if IP not in ownIPs:
						discoveredDevice = '{}: unknown device'.format(IP)
				if discoveredDevice is not None:	
					discoveredDevices.append(discoveredDevice)

discoverySocket.close()

if len(discoveredDevices) == 0:
	print('No devices found!')
else:
	print('The following devices responded within {} miliseconds:'.format(timeout * 1000))
	for discoveredDevice in discoveredDevices:
		print(discoveredDevice)
print('')