import re
import random

class customError(Exception):
	pass

def findAll(text, subText):
	start = 0
	while True:
		start = text.find(subText, start)
		if start == -1: return
		yield start
		start += len(subText)

def loadCustomTags(filename):
	customTags = dict()
	with open(filename,'r') as inFile:
		rawData = inFile.read().splitlines()
		for line in rawData:
			entry = dict()
			lineGroups = line.split(' = ')
			if len(lineGroups) != 2:
				raise customError('Invalid formatting detected in {}!'.format(filename))
			tag = lineGroups[0]
			while ' ' in tag: # remove whitespace in tags
				tag = tag.replace(' ', '')
			values = lineGroups[1]
			valueList = values.split(', ')
			includeValue = True
			if includeValue:
				customTags[tag] = valueList
	return customTags

def checkRequirements(entry, keywords):
	requirements = []
	for keyword in keywords:
		if keyword in entry:
			value = entry[keyword]
			requirement = len(value) > 0
			requirements.append(requirement)
	return requirements

def createValueDatabase(data, keywords):
	valueDatabase = set()
	extendedTagPattern = r'(<[^"<>]+>=)' # limits the first search to strings that end with a " = " (whitespace is optional)
	for entry in data:
		# check if all required strings are there
		requirementsFulfilled = checkRequirements(entry, keywords)
		if all(requirementsFulfilled):
			valueString = entry['values']
			#print('\n' + valueString)
			# find all tags in our valueString that satisfy the format <...> = 
			valueTags = re.findall(extendedTagPattern, valueString)
			valueTagIndices = list()
			if len(valueTags) > 0:
				for valueTag in valueTags:
					valueTagIndex = valueString.index(valueTag)
					valueTagIndices.append(valueTagIndex)
			# split apart the valueString at the beginning of these tags
			valueParts = list()
			if len(valueTagIndices) == 1:
				valueParts.append(valueString)
				#print(valueString)
			elif len(valueTagIndices) > 1:
				for valueTagID in range(len(valueTagIndices)):
					valueTagIndex = valueTagIndices[valueTagID]
					if valueTagID < len(valueTagIndices)-1:
						valueTagNextIndex = valueTagIndices[valueTagID + 1]
						valuePart = valueString[valueTagIndex:valueTagNextIndex]
					else:
						valuePart = valueString[valueTagIndex:]
					#print(valuePart)
					valueDatabase.add(valuePart)
	return valueDatabase

# find the command tags in valueString and make a list of their locations
def findTags(valueString, database, debugMessages = False):
	tagPattern = r'(<[^"<>]+>=)' # finds strings that start with <, end with >=, and contain at least one letter. The letters ", <, and > are not allowed.
	tags = re.findall(tagPattern, valueString)
	indices = None
	if len(tags) > 0:
		indices = list()
		for tag in tags:
			if tag in valueString:
				index = valueString.index(tag)
				indices.append(index)
				if debugMessages: # output for debugging
					print('Found {} in valueString at position {}!'.format(tag, index))
			# No match? try finding a match in the value database
			else:
				if database is None:
					if debugMessages: # output for debugging
						print('Could not find {} in valueString!')
				else:
					if debugMessages: # output for debugging
						print('Could not find {} in valueString! Searching for a database match...'.format(tag))
					matchCount = 0
					databaseString = None
					for databaseEntry in database:
						if tag in databaseEntry:
							matchCount += 1
							databaseString = databaseEntry
					if matchCount == 1:
						indices.append(len(valueString))
						valueString += databaseString
						if debugMessages: # output for debugging
							print('Exact database match found! Appending \"{}\" to valueString.'.format(databaseString))

					elif matchCount == 0:
						print('Warning: Found no matches for the tag {}. Skipping the associated command {}.'.format(tag, command))
					else:
						print('Warning: Found more than one match for the tag {}. Skipping the associated command {}.'.format(tag, command))
		indices.sort()
	# trim the trailing =
	for tagID in range(len(tags)):
		if tags[tagID].endswith('='):
			tags[tagID] = tags[tagID][:-1]
	return (tags, indices)

# split valueString at these locations into fragments
def splitString(tagIndices, tagString):
	stringParts = None
	if tagIndices is not None:
		if len(tagIndices) == 1:
			stringParts = [tagString]
		elif len(tagIndices) > 1:
			stringParts = list()
			for tagID in range(len(tagIndices)):
				tagIndex = tagIndices[tagID]
				if tagID < len(tagIndices)-1:
					tagNextIndex = tagIndices[tagID + 1]
					stringPart = tagString[tagIndex:tagNextIndex]
				else:
					stringPart = tagString[tagIndex:]
				stringParts.append(stringPart)
	return stringParts

# convert each string fragment into a dict
def parseFragments(fragments, debugMessages = False):
	tagPattern = r'(<[^"<>]+>)' # finds strings that start with <, end with >, and contain at least one letter. The letters ", <, and > are not allowed.
	definitionPattern = tagPattern[:-1] + r'=(.+))' # extends the tag search with a second group of free text
	entry = None
	if fragments is not None:
		if len(fragments) > 0:
			for fragment in fragments:
				if debugMessages: # output for debugging
					print('fragment: ', fragment)
				definitionMatch = re.match(definitionPattern, fragment)
				tagMatch = re.match(tagPattern, fragment)
				if definitionMatch:
					if entry is None:
						entry = dict()
					tag = tagMatch.group(1)
					values = definitionMatch.group(2)
					if debugMessages: # output for debugging
						print('tag: ', tag)
						print('values: ', values)

					# is this a list? if so, split it at the semicolons
					if ';' in values:
						subParts = values.split(';')
						entry[tag] = subParts
						if debugMessages: # output for debugging
							print('Found a list!')
					# otherwise, just use it as-is
					else:
						entry[tag] = values
				else:
					if debugMessages:
						print('Couldn\'t find pattern <...>=... in fragment {}!')
	return entry

# replace every command tag with a value from the value column
def replacePlaceholders(text, tags, entry, customTagLibrary, randomSeed, debugMessages = False):
	tagPattern = r'(<[^"<>]+>)' # finds strings that start with <, end with >, and contain at least one letter. The letters ", <, and > are not allowed.
	random.seed(randomSeed)
	if tags is not None:
		if len(tags) > 0:
			for tag in tags:
				if debugMessages: # output for debugging
					print('tag:',tag)
					print('entry:',entry)
				finalValue = None
				replaceSuccess = False
				if tag in entry:
					if debugMessages: # output for debugging
						print('found the tag {} in entry'.format(tag))
					values = entry[tag]
					if type(values) is str:
						valueTags = re.findall(tagPattern, values)
						if len(valueTags) == 0:
							# tag recognition failed, try looking for the raw string...
							if values in customTagLibrary:
								valueTags = [values]

						if debugMessages: # output for debugging
							print('{} is a String'.format(values))
							print('valueTags:', valueTags)

						for valueTag in valueTags:
							if valueTag in customTagLibrary:

								if debugMessages: # output for debugging
									print('valueTag is in customTagLibrary')

								randomValue = random.choice(customTagLibrary[valueTag])
								finalValue = values.replace(valueTag, randomValue)
								replaceSuccess = True
							else:
								if debugMessages: # output for debugging
									print('valueTag is not in customTagLibrary!')
						if not replaceSuccess:
							print('Warning: tag value {} lacks a custom entry. Please modify the file custom tag values.txt.'.format(values))
					elif type(values) is list:
						if debugMessages: # output for debugging
							print('{} is a List'.format(values))

						finalValue = random.choice(values)
						replaceSuccess = True
					else:
						raise customError('Error: Value for {} has the wrong format. Check the source code.'.format(commandNameString))
				else:
					if debugMessages: # output for debugging
						print('Couldn\'t find tag {} in entry!'.format(tag))
				if replaceSuccess:
					if text is not None:
						text = text.replace(tag, finalValue)
				else:
					raise customError('Error: couldn\'t find a replacement value for tag {}'.format(tag))
	return text