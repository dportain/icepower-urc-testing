# Icepower-URC-Testing

### A toolkit to interact with HDA devices over Ethernet

Swiftronix / Dominic Portain, 2020

## Installation

```
cd icepower-urc-testing
pip3 install -r requirements.txt
```

The software was tested with Python 3.7.x on MacOS, but should work on all unix-like systems and probably even on Windows. Definitely not compatible with Python 2.

## Contents

### discoverHDA.py

A standalone tool that discovers running HDA devices in the network.

It broadcasts a UDP package that consists of four sections, in this order:
* Sequence (a random number between 0 and 200, in two bytes)
* CmdID (the number 50000, in two bytes)
* ModelID (the number 0, in two bytes)
* our own IP (in four bytes)

Every device whose response contains the correct Sequence will be listed in the output.

There is a deactivated option in the code to specify a network interface. Per default, all interfaces are used. If the option is activated, running the script will require administrator privileges.

#### Usage

```
cd icepower-urc-testing
python3 discoverHDA.py

Querying devices...
The following devices responded within 500.0 miliseconds:
192.168.1.57: HDA_1600 (avb_souces: 2, avb_sinks: 1)
192.168.1.24: HDA_130 (avb_souces: 2, avb_sinks: 1)
192.168.1.197: HDA_IO (avb_souces: 2, avb_sinks: 1)
```


### main.py

An interface to verify the network functionality of HDA devices.

Per default, it sends five random commands to an HDA-8100 connected via the IP 192.168.1.160.
The responses to these commands are being evaluated and compared to the expected output listed in the specifications. If there is a mismatch, the query, the response and the expected response are displayed for debugging purposes. Otherwise a simple check mark is displayed.

All commands from the sheets "Accelerator System SET Cmds", "GET Polling Commands", "Macro Connected Device Cmds" and "UI Commands" are supported.
All output is also saved to a logfile automatically. A subfolder called `/log` will be created automatically.

#### Usage

```
cd icepower-urc-testing
python3 main.py --host 192.168.1.57 --devicename HDA_IO --number 1

SET_INPUT_3_AUDIO_SENSING_LEVEL
get: ✅ 
set: ✅ 
confirm: ❌
Error:
Sent command:         {"type":"set","input":{"id":"3","audio.sense":{"level":"0.40"}}}
Expected response: {"type":"status","input":{"id":"3","audio.sense":{"level":"0.40"}}}
Actual response:   {"type":"status","input":{"id":"3","audio.sense":{"level":"0.80"}}}
restore: ✅
```


#### Maintenance

When a new specification file is released, it should be placed in the subfolder `/config`. In all likelihood, the pre-configured filename will also have to be changed (see "less popular options" in the next section). When the new file is correctly configured, main.py will automatically detect the new specification file and analyze its contents (via the subroutine `specification.py`).
Because this analysis takes a couple of seconds, the results are written to cache files (in the subfolder `/temp`). These can be deleted safely at any time, provided that the specification file is still present.

Commands will be rejected (silently in random mode, less silently in manual mode) if they're not rated for the selected device. The code internally works with device groups that are read out from the first sheet of the specification file, and from the 'Units' column in all subsequent sheets. If you believe that your selected command should work on your selected device, please search for an inconsistency in the specification file first.

The specification file is notorious for containing multiple typos that lead to parsing erros. These are defined with a search/replacement pattern in specification.py -> convertExcel() -> typoFixes. If the author manages to add a new type of typo, this variable will have to be updated. The "r" before any given string means that special characters don't need to be escaped - useful in situations with a large amount of quotation marks.

Sometimes commands get disabled in the specification file. In Excel, the fields for these commands appear blacked out and the text is crossed out. Because I'm not sure if either formatting choice will stay consistent in the future, I'm ignoring it completely. If you need to disable new commands, update the file `config/disabled commands.txt` accordingly.

The specification file uses placeholders, for example **<gateway ip>**. These are replaced during runtime with a random value. These random values are selected from the file `config/custom tag values.txt`. In the off-chance that I included an invalid value, or if the author introduces a new placeholder, you will need to update this file accordingly. If you ever need to debug this functionality, the relevant code segment can be found in helpers.py -> replacePlaceholders(). Be sure to check the cached files in `/temp/` before working on that.


#### Options

Popular options can be accessed by setting an option during the terminal call.

```
--host:       IP address of the HDA device
--devicename: Name of the HDA device
--number:     The number of commands to be sent out in a row
--command:    The name of the command to be sent out (e.g. 'SET_INPUT_3_AUDIO_SENSING_LEVEL')
--debug:      Enable verbose pre-processing for one specific command name. Verbose processing only works when a valid command name is being supplied.

```

Less popular options need to be set in the source code.

```
xlsFilename:  The name of the original specification file
inFilenames:  The names of the cached intermediate files created by the specification parser
sendCommands: An option to send commands out over the network. If disabled, the commands are printed on screen instead. Enabled per default.
writeLogfile: An option to write the HDA responses to disk. Enabled per default.
```


### specification.py

Providing support functions for making the original specification file machine readable.

#### Main functions

`convertExcel()` - reads the original Excel file and writes out the important parts to cached files

`loadData()` - loads cached files into specifically structured variables

`prepareData()` - replace placeholders with random but valid values


### network.py

Providing support functions for sending out commands over the network.

#### Main functions

`sendSetCommand()` - a four-step process:
1. The HDA is asked for the current parameter of a particular command with a GET request
2. A SET command is sent with a new parameter
3. Another GET request is sent to verify that the new parameter has been set persistently
4. Another SET command restores the parameter to its original value

`sendGetCommand()` - queries the HDA with a GET request and compares the response with the expected value listed in the specification

`avertError()` - resolves trivial differences if there is a mismatch between expected and actual responses

`stripReplies()` - strips away the "reply" messages that contain no usable information


### helpers.py

Providing second-level support functions for various purposes. 80% of the code deals with JSON.