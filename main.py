import os
import json
import time
import nclib
import random
import argparse
import datetime

import specification
import network
import helpers

xlsFilename = './config/HDA Audio Network JSON Commands (4.18).xlsx'
inFilenames = {'set':'./temp/Accelerator System SET Cmds.txt',
			   'get':'./temp/GET Polling Commands.txt',
			   'xls':xlsFilename,
			   'customTags':'./config/custom tag values.txt',
			   'disabledCommands':'./config/disabled commands.txt'}

defaultHost = '192.168.1.160' # 176, 124, 83, 57, 44
port = 8002
sendCommands = True
writeLogfile = True

# parse command line arguments
parser = argparse.ArgumentParser(description='Send commands to the target HDA device')
parser.add_argument('--command', type=str, help='The name of the command to be sent out (default: random)', default=None)
parser.add_argument('--number', type=int, help='The number of commands to be sent out in a row (default: 5)', default=5)
parser.add_argument('--host', type=str, help='IP address of the HDA device', default=defaultHost)
parser.add_argument('--devicename', type=str, help='Name of the HDA device (e.g. HDA-8100)', default='HDA-8100')
parser.add_argument('--debug', type=str, help='Enable verbose pre-processing for one specific command name', default=None)
args = parser.parse_args()
manualCommand = args.command
commandCount = args.number
host = args.host
deviceName = args.devicename
debugCommandName = args.debug

# load commands from the Excel sheet, populating placeholders with random values
commandSets, productGroups, zoneIdentifiers = specification.loadData(inFilenames, deviceName, debugCommandName)

# structure of commandSets:
# commandSets (dict, indexed with 'set' and 'get')
# -> commandSet (dict, indexed with one of the command names)
# -> commandData (dict, indexed by 'command' or 'response')
# -> command (string) / response (string)

# build a list of command names
commandNames = list()
commandSet = {}
while len(commandNames) < commandCount: # select random commands from every command set
	for keyword in commandSets:
		commandSet = commandSets[keyword]
		if manualCommand is None: # the user wants random commands
			commandName = random.choice(list(commandSet))
		else: # the user wants a manual command that needs to be added a number of times
			if manualCommand in commandSet:
				commandName = manualCommand
		# check if this command is valid for this device
		productGroup = commandSet[commandName]['device']
		if productGroup in productGroups:
			# if so, add it to the list
			commandNames.append(commandName)

# load custom tag definition file
customTags = helpers.loadCustomTags(inFilenames['customTags'])

# build a stack of commands based on these names
commandStack = list()
for commandName in commandNames:
	for keyword in commandSets:
		commandSet = commandSets[keyword]
		if commandName in commandSet:
			rawCommandData = commandSet[commandName]
			# replace placeholders with random but valid values
			commandData = specification.prepareData(rawCommandData, customTags, commandName == debugCommandName)
			commandStack.append(commandData)


# Prepare sending out commands over the network
connection = None
status = None
logfile = None

# test if the device is reachable for ping requests
if sendCommands:
	pingCommand = 'ping -c 1 -W 1000 {} > /dev/null'.format(host)
	response = os.system(pingCommand)

	# ping successful, open a connection
	if response == 0:
		print('Opening connection to {}...'.format(host))
		connection = nclib.Netcat((host, port))
		connection.settimeout(0.5)
		print('...success!')

		# and a logfile
		if writeLogfile:
			if not os.path.exists('./log/'):
				os.mkdirs('./log')
			logfileName = './log/' + datetime.datetime.now().strftime('%y-%m-%d %H-%M-%S') + '.log'
			logfile = open(logfileName,'w')
	else:
		print('Couldn\'t establish a connection to {}!'.format(host))
		connection = None


# send out the commands over the network
if sendCommands:
	# has the connection been established?
	if connection is not None:
		for commandData in commandStack:
			# test if the device is still there
			response = os.system(pingCommand)
			if response == 0:
				# yup, send out command
				debugMessages = commandData['name'] == debugCommandName
				status = network.sendCommand(connection, commandData, debugMessages=debugMessages)
			else:
				print('Lost connection with the HDA!')
				break # exit "for" loop
			# write errors to a log file
			if writeLogfile:
				logfile.write('\n' + commandData['name'] + '\n')
				for keyword in status:
					logfile.write('{}: {}\n'.format(keyword, repr(status[keyword])))
					if not status[keyword].ok:
						if status[keyword].statusText is not None:
							logfile.write(status[keyword].statusText + '\n')
	
else:
	for commandData in commandStack:
		command = commandData['command']
		response = commandData['response']
		
		print('\n' + commandName)
		print(' Command String: {}'.format(command))
		print('Response String: {}'.format(response))

if sendCommands:
	if connection is not None:
		print('...closing connection to {}.'.format(host))
		connection.close()

if logfile is not None:
	logfile.close()
		