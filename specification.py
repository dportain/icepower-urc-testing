import openpyxl
import json
import random
import os

import helpers


class customError(Exception):
	pass


def extractProductGroups(table):
	productGroups = {}
	for row in table:
		cellText = row[0].value
		textFragments = cellText.split(': ')
		if len(textFragments) == 2:
			productGroup = textFragments[0]
			modelText = textFragments[1]
			models = modelText.split(', ')
			for model in models:
				if model not in productGroups:
					productGroups[model] = []
				productGroups[model].append(productGroup)
		else:
			raise customError('Input data in the wrong format. Expected only one colon per line.')

	# plausibility checks
	allGroups = set()
	for model in productGroups:
		allGroups.update(productGroups[model])
	if len(allGroups) != len(table):
		print(allGroups)
		raise customError('Could not extract all product groups from the specification file! The previously listed product groups were extracted.')		
		
	# example output format:
	# productGroups = {'HDA-130': ['G-1', 'G-2', 'G-3', 'G-5', 'G-7', 'G-15'], 'HDA-8100': ...}
	return productGroups


def extractZoneIdentifiers(table):
	zoneIdentifiers = {}
	for row in table:
		cellText = row[0].value
		textFragments = cellText.split(': ')
		if len(textFragments) == 2:
			model = textFragments[0]
			zoneText = textFragments[1]
			zones = zoneText.split('; ')
			zoneIdentifiers[model] = zones

	# plausibility checks
	models = list(zoneIdentifiers)
	if len(models) != len(table):
		print(models)
		raise customError('Could not extract all zone identifiers from the specification file! The previously listed devices were extracted.')
		
		# if you were coming here because of the error message: maybe the location for the definition has changed? It's hard-coded in the call to this function
	if not zoneIdentifiers:
		raise customError('Extracting the zone identifiers from the specification file failed completely!')

	# example output format:
	# zoneIdentifiers = {'HDA-1600-70v': ['z1lr', 'z1l', 'z1r'], 'HDA-130': ...}
	return zoneIdentifiers


def convertExcel(xlsFilename):
	reportInputErrors = False
	outputJSON = False
	outputTXT = True
	outFilenameTXT = './temp/{}.txt'
	outFilenameJSON = './temp/{}.json'

	# --- every entry in the next three variables describes one worksheet

	worksheetNames = [
		r'Accelerator System "SET" Cmds',
		r'"GET" Polling Commands',
		r'Macro Connected Device Cmds',
		r'UI Commands'
	]

	columnTitleMaps = [
		{'Units':'device', 'Command Name ':'commandName', 'JSON Command':'JSON', 'Variable Permitted Values':'values', 'GET_CONFIGURATION_DATA     HDA Device Response ("GET" Polling Commands sheet, line 292)':'response'},
		{'Units':'device', 'Command Name ':'commandName', 'JSON Command':'JSON', 'Variable Permitted Values':'values', 'HDA Device Response':'response'},
		{'Units':'device', 'Command Name ':'commandName', 'JSON Command':'JSON', 'Variable Permitted Values':'values', 'HDA Device Response':'response'},
		{'Units':'device', 'Command Name ':'commandName', 'JSON Command':'JSON', 'Variable Permitted Values':'values', 'HDA Device Response':'response'},
	]

	headerRowIDs = [3, 3, 3, 4]

	# --- end of workbook grouping

	# Define all typos and their fixes in the Excel file
	typoFixes = {
		'“' : '\"',
		'”' : '\"',
		r'"mic"{' : r'"mic":{',
		r'{"id":<mic #>"' : r'{"id":"<mic #>"',
		r'"limiter";{"' : r'"limiter":{"',
		r'""system"' : r'"system"',
		r'"mic","id":"' : r'"mic":{"id":"',
		r'"eq.music"":' : r'"eq.music":',
		r'"zone":{"id":"<zone #>""echo":' : r'"zone":{"id":"<zone #>","echo":',
		r'"device":["<mac addr>":"<value>"]' : r'"device":{"<mac addr>":"<value>"}',
		r'"device":["mac":"<mac addr>","h.ver":"<hardware ver>","f.ver":"<firmware ver>","serial":"<serial #>","os.ver":"<os ver>"]' : r'"device":{"mac":"<mac addr>","h.ver":"<hardware ver>","f.ver":"<firmware ver>","serial":"<serial #>","os.ver":"<os ver>"}'
	}

	typoIgnored = [
		'\"function\":{\"echo.clear\"}',
		'\"function\":{\"echo.status\"}',
		'\"function\":{\"echo.format\"}',
		'\"function\":{\"control.type\"}',
		'\"function\":{\"all.status\"}',
		'\"function\":{\"all.format\"}',
		'\"function\":{\"info\"}',
		'\"system\":{\"all.status\"}'
	]

	print('Updating cached command files...')

	# Open Excel workbook
	workbook = openpyxl.load_workbook(filename = xlsFilename)

	# extract hardware product groups
	productGroups = extractProductGroups(workbook['Overview']['C23:C37'])
	with open(outFilenameJSON.format('productGroups'), 'w') as outFile:
		json.dump(productGroups, outFile)

	zoneIdentifiers = extractZoneIdentifiers(workbook['Overview']['C48:C53'])
	with open(outFilenameJSON.format('zoneIdentifiers'), 'w') as outFile:
		json.dump(zoneIdentifiers, outFile)


	# Load main data entries from workbooks listed in worksheetNames
	for worksheetID, worksheetName in enumerate(worksheetNames):
		print('Loading worksheet \"{}\"...'.format(worksheetName))
		worksheet = workbook[worksheetName]
		filename = worksheetName.replace('\"', '')

		# Parse the header row and identify the interesting columns by name
		headerData = dict()
		headerRowID = headerRowIDs[worksheetID]
		headerRow = worksheet[headerRowID]
		headerEntries = [cell.value for cell in headerRow]
		columnTitleMap = columnTitleMaps[worksheetID]
		foundItems = []
		for headerIndex, headerEntry in enumerate(headerEntries):
			if headerEntry is not None:
				if headerEntry in columnTitleMap:
					columnTitle = columnTitleMap[headerEntry]
					headerData[columnTitle] = headerEntries.index(headerEntry)
					foundItems.append(columnTitle)
		columnTitles = list(headerData)
		if len(foundItems) < len(columnTitles):
			print('Successful match between the following items and the variable columnTitleMap:')
			for foundItem in foundItems:
				print(foundItem)
			raise customError('Could not match all column titles in the Excel sheet to the ones in the variable columnTitleMap.')

		# Import the interesting data columns
		data = []
		if len(headerData) > 0:
			for rowID, row in enumerate(worksheet.values):
				if rowID > headerRowID - 1:
					dataEntry = {}
					for headerEntry in columnTitles:
						columnIndex = headerData[headerEntry]
						value = row[columnIndex]
						if value is not None:
							dataEntry[headerEntry] = value
					if dataEntry:
						data.append(dataEntry)

		# data is a list of entries
		# every entry is a dict with the format {columnTitle: value, columnTitle: value, ...}

		# Parse the JSON strings (while checking and correcting for typos)
		print('Parsing and correcting JSON strings...')
		typosList = list(typoFixes)
		jsonObjects = {'JSON':[], 'response':[]}
		if len(data) > 0:
			for keyword in jsonObjects.keys():
				for lineID, entry in enumerate(data):
					jsonDict = None
					if keyword in entry:
						jsonDict = dict()
						jsonString = entry[keyword]
						if jsonString is not None:
							if jsonString.startswith(r'{"type"'):
								ignoredRow = False
								for typo in typoIgnored:
									if typo in jsonString:
										ignoredRow = True
								if not ignoredRow:
									for typoID, typo in enumerate(typosList):
										while typo in jsonString:
											correction = typoFixes[typo]
											jsonString = jsonString.replace(typo, correction)
									try:
										jsonDict = json.loads(jsonString) # this parser quits with an error if the syntax is wrong
									except e:
										print('JSON string could not be parsed: {}'.format(jsonString))
										print('Please find the error and include a patch in the typoFixes variable.')
										raise(e)
					jsonObjects[keyword].append(jsonDict)

		# Print out the commands as text file
		print('Writing out JSON commands as text file...')
		with open(outFilenameTXT.format(filename),'w') as outFile:
			outFile.write('\t'.join(columnTitles) + '\n')
			if len(data) > 0:
				for jsonCommand, jsonResponse, entryRow in zip(jsonObjects['JSON'], jsonObjects['response'], data):
					#print(entryRow)
					entries = []
					for columnTitle in columnTitles:
						# if we hit the JSON column, use the corrected entry
						if columnTitle == 'JSON':
							if jsonCommand == None:
								entry = None
							else:
								entry = json.dumps(jsonCommand)
						elif columnTitle == 'response':
							if jsonResponse == None:
								entry = None
							else:
								entry = json.dumps(jsonResponse)
						else:
							if columnTitle in entryRow:
								entry = entryRow[columnTitle]
							else:
								#print('Couldn''t find expected columnTitle in entryRow ({})'.format(list(entryRow)))
								entry = None
						if entry is None:
							entries.append('')
						else:
							while('\n' in entry):
								entry = entry.replace('\n', ' ')
							while('\t' in entry):
								entry = entry.replace('\t', '    ')
							entries.append(entry)
							
					outFile.write('\t'.join(entries) + '\n')

	print('...finished.')


def loadData(inFilenames, deviceName, debugCommandName):

	requiredKeywordSet = {'set':['device', 'commandName', 'JSON', 'values', 'response'], 'get':['device', 'commandName', 'JSON', 'response']}
	filePatternJSON = './temp/{}.json'

	zonesFilename = filePatternJSON.format('zoneIdentifiers')
	productsFilename = filePatternJSON.format('productGroups')

	# check if the cached files are there and generate them if necessary
	xlsFilename = inFilenames['xls']
	del(inFilenames['xls'])
	inFilesExist = []
	for key in inFilenames:
		inFilename = inFilenames[key]
		inFilesExist.append(os.path.isfile(inFilename))
	inFilesExist.append(os.path.isfile(zonesFilename))
	inFilesExist.append(os.path.isfile(productsFilename))
	if not all(inFilesExist):
		convertExcel(xlsFilename)

	# load product groups
	with open(productsFilename, 'r') as inFile:
		allProductGroups = json.load(inFile)
	if deviceName in list(allProductGroups):
		productGroups = allProductGroups[deviceName]
	else:
		print('Product name not recognized. Valid product names: {}'.format(', '.join(list(allProductGroups))))

	# load zone identifiers
	with open(zonesFilename, 'r') as inFile:
		allZoneIdentifiers = json.load(inFile)
	zoneIdentifiers = allZoneIdentifiers[deviceName]

	# load disabled commands
	disabledCommandNames = []
	with open(inFilenames['disabledCommands'],'r') as inFile:
		disabledCommandNames = inFile.read().splitlines()

	# update cached command files if there is a new spec file
	xlsFiledate = os.path.getmtime(xlsFilename)
	inFiledates = []
	for key in requiredKeywordSet:
		inFilename = inFilenames[key]
		inFiledate = os.path.getmtime(inFilename)
		inFiledates.append(inFiledate)
	if any([xlsFiledate > inFiledate for inFiledate in inFiledates]):
		convertExcel(xlsFilename)

	commandSets = {}
	for key in requiredKeywordSet:
		inFilename = inFilenames[key]
		requiredKeywords = requiredKeywordSet[key]

		# load raw command file
		rawData = None
		with open(inFilename, 'r') as inFile:
			rawData = inFile.read().splitlines()

		# convert lines of strings to a list of dicts
		data = []
		if rawData is not None:
			columnTitles = []
			keyColumnIndex = None
			for rowID, row in enumerate(rawData):
				# read the header
				if rowID == 0:
					columnTitles = row.split('\t')
				# convert each subsequent line into a dict
				else:
					entry = {}
					rawEntries = row.split('\t')
					for columnTitle, rawEntry in zip(columnTitles, rawEntries):
						# remove all whitespace
						while ' ' in rawEntry:
							rawEntry = rawEntry.replace(' ','')
						entry[columnTitle] = rawEntry
					commandName = entry['commandName']
					if debugCommandName == commandName:
						print('\nPhase 0:')
						print('entry:',entry)
					if commandName not in disabledCommandNames:
						data.append(entry)

		# extract value tags and make a database of all value strings
		if key == 'set':
			valueDatabase = helpers.createValueDatabase(data, requiredKeywords)
		else:
			valueDatabase = None

		# parse the "Variable permitted values" column
		commandSet = dict()
		# match command tags and value tags, then convert value strings into dicts
		for entry in data:

			# check if all required strings are there
			requirementsFulfilled = helpers.checkRequirements(entry, requiredKeywords)
			valueEntry = None
			commandName = entry['commandName']
			debugMessages = commandName == debugCommandName

			if all(requirementsFulfilled):
				rawCommand = entry['JSON']
				valueString = entry['values']

				# find the command tags in valueString and make a list of their locations
				if debugMessages:
					print('\nStage 1a: finding command tags in valueString')
					print('commandName:', commandName)
					print('valueString:', valueString)
					print('rawCommand:', rawCommand)
				(commandTags, commandTagIndices) = helpers.findTags(valueString, valueDatabase, debugMessages=debugMessages)
				entry['commandTags'] = commandTags

				# split valueString at these locations into fragments
				if debugMessages:
					print('commandTagIndices:', commandTagIndices)
					print('\nStage 1b: splitting up valueString into valueFragments')
					print('valueString:', valueString)
				valueFragments = helpers.splitString(commandTagIndices, valueString)

				# convert each fragment into a dict
				if debugMessages:
					print('valueFragments:', valueFragments)
					print('\nStage 1c: converting valueStrings into dicts')
				valueEntry = helpers.parseFragments(valueFragments, debugMessages=debugMessages)
				# if the conversion succeeded, replace the old value entry with the new one
				if valueEntry is not None:
					entry['values'] = valueEntry

				# store the enhanced entry in a dict
				commandSet[commandName] = entry

			else:
				if debugMessages:
					print('\nStage 1 (failed):')
					print(commandName)
					print('Fulfilled requirements:')
					for req, keyword in zip(requirementsFulfilled, keywords):
						print(keyword, req)
					print(entry)

		commandSets[key] = commandSet
	return commandSets, productGroups, zoneIdentifiers


def prepareData(entry, customTags, debugMessages):
	# find placeholder tags in the JSON command and replace them with concrete values
	rawCommand = entry['JSON']
	valueEntry = entry['values']
	commandTags = entry['commandTags']
	commandName = entry['commandName']
	rawResponse = entry['response']
	productGroup = entry['device']

	# Generate a random number used for filling placeholder fields
	randomSeed = random.randint(0, 10000)

	# replace every command tag with a value from the value column
	if debugMessages:
		print('\nStage 2a')
		#print('entry:',entry)
		print('rawCommand:', rawCommand)
		print('commandTags:', commandTags)
		print('valueEntry:', valueEntry)
	command = helpers.replacePlaceholders(rawCommand, commandTags, valueEntry, customTags, randomSeed, debugMessages = debugMessages)

	# extract expected network response
	if debugMessages:
		print('\nStage 2b')
		print('entry:', entry)
		print('rawResponse:', rawResponse)
	response = helpers.replacePlaceholders(rawResponse, commandTags, valueEntry, customTags, randomSeed, debugMessages = debugMessages)
	if debugMessages:
		print('response:', response)

	newEntry = {'name':commandName, 'command':command, 'response':response, 'productGroup':productGroup}

	return newEntry